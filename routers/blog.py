from fastapi import APIRouter, Depends, status, Response, HTTPException
from schemas import ShowBlog, BlogBase, Blog
from typing import List
from database import get_db
import models
from sqlalchemy.orm import Session
from .auth import get_current_user


router = APIRouter(
    tags=["Blogs"],
    prefix="/blog"
)


@router.get("/", response_model=List[Blog])
def get_blogs(db: Session = Depends(get_db), current_user: models.User = Depends(get_current_user)):
    blogs = db.query(models.Blog).all()
    return blogs


@router.post("/", status_code=status.HTTP_201_CREATED)
def create(request: BlogBase, db: Session = Depends(get_db)):
    new_blog = models.Blog(title=request.title, body=request.body, user_id=1)
    db.add(new_blog)
    db.commit()
    db.refresh(new_blog)
    return new_blog


@router.get("/{id}", status_code=200, response_model=Blog)
def show(response: Response, id, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id == id).first()
    if not blog:
        # response.status_code = status.HTTP_404_NOT_FOUND
        # return {'detail': f'Blog with the ID {id} is not available.'}
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Blog with the ID {id} is not available.')
    return blog


@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def destroy(id, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id == id)
    if not blog.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Blog with the ID {id} is not available.')
    blog.delete(synchronize_session=False)
    db.commit()
    return {'data': 'done!!!!'}


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: Blog, db: Session = Depends(get_db)):
    # import pdb;pdb.set_trace()
    blog = db.query(models.Blog).filter(models.Blog.id == id)
    if not blog.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Blog with the ID {id} is not available.')
    print(request)
    blog.update({'title': request.title, 'body': request.body}, synchronize_session=False)
    db.commit()
    return {'data': 'Blog updated!!!!'}
