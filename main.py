from fastapi import FastAPI, Depends, status, Response, HTTPException
from schemas import Blog, ShowBlog, User, ShowUser, Login
import models
from database import engine, SessionLocal, get_db
from sqlalchemy.orm import Session
from typing import List
from passlib.context import CryptContext
from routers import blog, user, auth

app = FastAPI()

app.include_router(blog.router)
app.include_router(user.router)
app.include_router(auth.router)

models.Base.metadata.create_all(engine)

# def get_db():
# 	db = SessionLocal()
# 	try:
# 		yield db
# 	finally:
# 		db.close()


# @app.post("/blog", status_code=status.HTTP_201_CREATED, tags=["blogs"])
# def create(request: Blog, db: Session = Depends(get_db)):
# 	new_blog = models.Blog(title=request.title, body=request.body, user_id = 1)
# 	db.add(new_blog)
# 	db.commit()
# 	db.refresh(new_blog)
# 	return new_blog
#   return request


# @app.get("/blog", response_model = List[ShowBlog], tags=["blogs"])
# def get_blogs(db: Session = Depends(get_db)):
# 	blogs = db.query(models.Blog).all()
# 	return blogs


# @app.get("/blog/{id}", status_code = 200, response_model = ShowBlog, tags=["blogs"])
# def show(response: Response, id, db: Session = Depends(get_db)):
# 	blog = db.query(models.Blog).filter(models.Blog.id==id).first()
# 	if not blog:
# 		# response.status_code = status.HTTP_404_NOT_FOUND
# 		# return {'detail': f'Blog with the ID {id} is not available.'}
# 		raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, 
# 			detail=f'Blog with the ID {id} is not available.')
# 	return blog


# @app.delete("/blog/{id}", status_code = status.HTTP_204_NO_CONTENT, tags=["blogs"])
# def destroy(id, db: Session = Depends(get_db)):
# 	blog = db.query(models.Blog).filter(models.Blog.id == id)
# 	if not blog.first():
# 		raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
# 		 detail=f'Blog with the ID {id} is not available.')
# 	blog.delete(synchronize_session=False)
# 	db.commit()
# 	return {'data': 'done!!!!'}


# @app.put('/blog/{id}', status_code=status.HTTP_202_ACCEPTED, tags=["blogs"])
# def update(id, request: Blog, db: Session = Depends(get_db)):
# 	# import pdb;pdb.set_trace()
# 	blog = db.query(models.Blog).filter(models.Blog.id == id)
# 	if not blog.first():
# 		raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
# 		 detail=f'Blog with the ID {id} is not available.')
# 	print(request)
# 	blog.update({'title': request.title, 'body': request.body}, synchronize_session=False)
# 	db.commit()
# 	return {'data': 'Blog updated!!!!'}


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


# @app.post("/user", status_code=status.HTTP_201_CREATED, response_model=ShowUser, tags=["users"])
# def create_user(request: User, db: Session = Depends(get_db)):
# 	hashed_password = pwd_context.hash(request.password)
# 	new_user = models.User(name=request.name, email=request.email, password=hashed_password)
# 	db.add(new_user)
# 	db.commit()
# 	db.refresh(new_user)
# 	return new_user


# @app.get("/user/{id}", response_model=ShowUser, tags=["users"])
# def get_user(id, db: Session = Depends(get_db)):
# 	user = db.query(models.User).filter(models.User.id == id).first()
# 	if not user:
# 		raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
# 			detail=f'User with the ID {id} is not available.')
# 	return user


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

# @app.post("/login")
# def login(request: Login, db: Session = Depends(get_db)):
# 	# import pdb; pdb.set_trace()
# 	hashed_password = pwd_context.hash(request.password)
# 	user = db.query(models.User).filter(models.User.email == request.username).first()

# 	if not user:
# 		raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, 
# 			detail="Invalid Credentials!!!!")

# 	if not verify_password(request.password, user.password):
# 		raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, 
# 			detail=" Please enter correct password.!!!!")

# 	return user
